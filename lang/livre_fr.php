<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_livre' => 'Ajouter ce livre',

	// C
	'caracteristiques' => 'Caractéristiques',
	'champ_collection_label' => 'Collection',
	'champ_date_parution_label' => 'Date de parution',
	'champ_date_nouvelle_edition_label' => 'Date nouvelle édition',
	'champ_editeur_label' => 'Éditeur(s)',
	'champ_edition_label' => 'Mention nouvelle édition',
	'champ_extrait_label' => 'Extrait',
	'champ_hauteur_explication' => 'en cm',
	'champ_hauteur_label' => 'Hauteur',
	'champ_infos_sup_label' => 'Infos supplémentaires',
	'champ_isbn_label' => 'ISBN',
	'champ_largeur_explication' => 'en cm',
	'champ_largeur_label' => 'Largeur',
	'champ_pages_label' => 'Nombre de pages',
	'champ_poids_explication' => 'En grammes',
	'champ_poids_label' => 'Poids',
	'champ_prix_explication' => 'Sans le symbole €',
	'champ_prix_label' => 'Prix',
	'champ_reliure_explication' => 'Broché, collé, etc.',
	'champ_reliure_label' => 'Type reliure',
	'champ_sommaire_label' => 'Sommaire',
	'champ_soustitre_label' => 'Soustitre',
	'champ_texte_label' => 'Présentation',
	'champ_titre_label' => 'Titre',
	'champ_traduction_label' => 'Traduit de…',
	'champ_volume_label' => 'Volume',
	'confirmer_supprimer_livre' => 'Confirmez-vous la suppression de cet livre ?',

	// I
	'icone_creer_livre' => 'Créer un livre',
	'icone_modifier_livre' => 'Modifier ce livre',
	'info_1_livre' => 'Un livre',
	'info_aucun_livre' => 'Aucun livre',
	'info_livres_auteur' => 'Les livres de cet auteur',
	'info_nb_livres' => '@nb@ livres',
	'info_livres_proposes' => 'Livres à paraître',

	// R
	'retirer_lien_livre' => 'Retirer ce livre',
	'retirer_tous_liens_livres' => 'Retirer tous les livres',
	'role_grand_format' => 'Grand format',
	'role_poche' => 'Poche',
	'role_couverture' => 'Couverture',
	'role_4couverture' => '4e de couv',

	// S
	'supprimer_livre' => 'Supprimer cet livre',

	// T
	'texte_ajouter_livre' => 'Ajouter un livre',
	'texte_changer_statut_livre' => 'Ce livre est :',
	'texte_creer_associer_livre' => 'Créer et associer un livre',
	'texte_date_parution' => 'Date de parution',
	'texte_definir_comme_traduction_livre' => 'Ce livre est une traduction du livre numéro :',
	'texte_date_nouvelle_edition_nonaffichee' => 'Ne pas afficher la date de nouvelle publication',
	'texte_statut_aparaitre' => 'à paraître',
	'texte_statut_paru' => 'paru',
	'texte_statut_parus' => 'Parus',
	'texte_statut_epuise' => 'épuisé',
	'texte_statut_epuises' => 'Épuisés',
	'texte_livres_recents' => 'Livres les plus récents',
	'titre_langue_livre' => 'Langue de ce livre',
	'titre_livre' => 'Livre',
	'titre_livres' => 'Livres',
	'titre_livres_rubrique' => 'Livres de la rubrique',
	'titre_logo_livre' => 'Couverture livre',
	'titre_objets_lies_livre' => 'Liés à ce livre',
);
