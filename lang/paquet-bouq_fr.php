<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouq_description' => 'Ce plugin se veut être le plus petit dénominateur commun pour une gestion de livres et de leurs auteurs. Il devrait pouvoir être utile aux cas suivants :

		- Maisons d’édition de livre,<br>
		- Librairies,<br>
		- Bibliothèques,<br>
		- Boutiques en ligne de vente de livre,<br>
		- etc.',
	'bouq_nom' => 'Bouquinerie',
	'bouq_slogan' => 'Gestion de livres et de leurs auteurs'
	
);
