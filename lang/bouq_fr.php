<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouq_titre' => 'Bouquinerie',

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_contenu_livres' => 'Contenus des livres',
	'cfg_titre_contenu_auteurs' => 'Contenus des auteurs de livre',

	// T
	'titre_page_configurer_bouq' => 'Bouquinerie',
	'texte_contenus' => 'Selon la maquette adoptée pour votre site, vous pouvez décider que certains éléments des livres ou auteurs de livre ne sont pas utilisés. Utilisez les listes ci-dessous pour indiquer quels éléments sont disponibles.',
);
